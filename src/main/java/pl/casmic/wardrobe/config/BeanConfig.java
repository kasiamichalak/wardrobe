package pl.casmic.wardrobe.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import pl.casmic.wardrobe.WardrobeApplication;
import pl.casmic.wardrobe.accuweather.model.Location;
import pl.casmic.wardrobe.accuweather.model.WeatherConditions;
import pl.casmic.wardrobe.accuweather.util.ApiKey;

@Configuration
public class BeanConfig {

    private static final Logger logger = LoggerFactory.getLogger(WardrobeApplication.class);
    private String postcode = "00-377";

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner runLocation(RestTemplate restTemplate) {
        return args -> {
            String url = "http://dataservice.accuweather.com/locations/v1/cities/search?q="
                    + postcode + "&apikey=" + ApiKey.getKey();
            logger.info(url);
            Location[] locations = restTemplate.getForObject(url, Location[].class);
            for (Location location: locations) {
                logger.info(location.toString());
            }
        };
    }

    @Bean
    public CommandLineRunner runCurrentWeatherConditions(RestTemplate restTemplate) {

//      Location key for postcode 00-377 is '332744_PC'
        String locationKey = "332744_PC";
        return args -> {
            String url = "http://dataservice.accuweather.com/currentconditions/v1/"
                    + locationKey + "?apikey=" + ApiKey.getKey() + "&details=true";
            logger.info(url);
            WeatherConditions[] conditionsArray = restTemplate.getForObject(url, WeatherConditions[].class);
            for (WeatherConditions conditions: conditionsArray) {
                logger.info(conditions.toString());
            }
        };
    }
}
