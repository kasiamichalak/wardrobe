package pl.casmic.wardrobe.accuweather.service;

public interface ParsingService<T> {

    public T parseObject(String url);

    public T[] parseObjectsArray(String url);
}
