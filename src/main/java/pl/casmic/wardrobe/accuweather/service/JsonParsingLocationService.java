package pl.casmic.wardrobe.accuweather.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.casmic.wardrobe.accuweather.model.Location;

@Service
public class JsonParsingLocationService implements ParsingService<Location> {

    private RestTemplate restTemplate;

    public JsonParsingLocationService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Location parseObject(String url) {
        return restTemplate.getForObject(url, Location.class);
    }

    @Override
    public Location[] parseObjectsArray(String url) {
        return restTemplate.getForObject(url, Location[].class);
    }
}
