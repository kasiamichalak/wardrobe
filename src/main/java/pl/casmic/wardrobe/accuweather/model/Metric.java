package pl.casmic.wardrobe.accuweather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties
@Getter
@Setter
@ToString
public class Metric {

    @JsonProperty("Value")
    private double value;
    @JsonProperty("Unit")
    private String unit;

}
