package pl.casmic.wardrobe.accuweather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = false)
@Getter
@Setter
@ToString
public class WeatherConditions {

//    @JsonProperty("LocalObservationDateTime")
//    private LocalDateTime localDateTime;
    @JsonProperty("WeatherText")
    private String description;
    @JsonProperty("WeatherIcon")
    private int icon;
    @JsonProperty("HasPrecipitation")
    private boolean hasPrecipitation;
    @JsonProperty("PrecipitationType")
    private String precipitationType;
    @JsonProperty("ApparentTemperature")
    private ApparentTemperature apparentTemperature;

}
