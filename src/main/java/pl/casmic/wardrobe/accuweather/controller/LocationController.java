package pl.casmic.wardrobe.accuweather.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.casmic.wardrobe.accuweather.model.Location;
import pl.casmic.wardrobe.accuweather.service.ParsingService;
import pl.casmic.wardrobe.accuweather.util.ApiKey;

import java.util.Arrays;
import java.util.List;

@RestController
public class LocationController {

    private ParsingService parsingService;

    public LocationController(ParsingService parsingService) {
        this.parsingService = parsingService;
    }

    @GetMapping("/location/{postcode}")
    public Location getLocationByPostcode(@PathVariable String postcode) {
        Location[] locations = (Location[]) parsingService.parseObjectsArray(
                "http://dataservice.accuweather.com/locations/v1/cities/search?q=" +
                        postcode + "&apikey=" + ApiKey.getKey());
        Location location = locations[0];
        return location;
    }
}
