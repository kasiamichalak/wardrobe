package pl.casmic.wardrobe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WardrobeApplication {

    private static final Logger logger = LoggerFactory.getLogger(WardrobeApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(WardrobeApplication.class, args);
    }

}
